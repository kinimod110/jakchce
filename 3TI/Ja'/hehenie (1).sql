-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 23 Paź 2019, 08:21
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `hehenie`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategoria`
--

CREATE TABLE `kategoria` (
  `ID` int(16) NOT NULL,
  `Tiaa2` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `kategoria`
--

INSERT INTO `kategoria` (`ID`, `Tiaa2`) VALUES
(1, 'Pierwsza'),
(2, 'Druga'),
(3, 'Trzecia'),
(6, '33'),
(8, 'dsfefd');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klienci`
--

CREATE TABLE `klienci` (
  `ID` int(10) NOT NULL,
  `Imie` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Nazwisko` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `klienci`
--

INSERT INTO `klienci` (`ID`, `Imie`, `Nazwisko`, `email`) VALUES
(1, 'Jarek', 'Darek', 'dasd@gmail.pl'),
(2, 'Dria', 'Anna', 'asd@gmail.pl'),
(3, 'Mariusz', 'Waclaw', 'dwdaasd@gmail.pl'),
(4, 'Judasz', 'Janusz', 'dasddawd@gmail.pl'),
(5, 'Monika', 'Kozimierz', 'dasdawdasd@gmail.pl');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkty`
--

CREATE TABLE `produkty` (
  `ID` int(10) NOT NULL,
  `Tiaa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Cena` float NOT NULL,
  `Kategoria` int(10) NOT NULL,
  `Opis` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkty`
--

INSERT INTO `produkty` (`ID`, `Tiaa`, `Cena`, `Kategoria`, `Opis`) VALUES
(1, 'Pomidor', 2.2, 1, 'To jest Jedzenie'),
(2, 'Jabko', 1.8, 1, 'To jest Jedzenie'),
(3, 'Salata', 2.6, 1, 'To jest Jedzenie'),
(4, 'Papierosy', 21.6, 2, 'To jest Przyjemnosc'),
(5, 'Ziemniaki', 11.1, 1, 'To jest Jedzenie'),
(6, 'Dlugopis', 1.25, 3, 'To jest Przedmiot'),
(7, 'Joint', 100, 2, 'To jest Przyjemnosc'),
(8, 'Leki', 1000, 2, 'To jest Przyjemnosc'),
(9, 'Meta', 92, 2, 'To jest Przyjemnosc'),
(10, 'Wiadro', 66, 2, 'To jest Przyjemnosc');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `klienci`
--
ALTER TABLE `klienci`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `produkty`
--
ALTER TABLE `produkty`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `ID` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `klienci`
--
ALTER TABLE `klienci`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `produkty`
--
ALTER TABLE `produkty`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>tabela PHP</title>
    <?php
    $polaczenie = mysqli_connect("localhost","root","","tabela_dane") or die("błąd połączenie");
    mysqli_query($polaczenie, "SET CHARSET utf8");
    ?>
    <style type="text/css">
    h1 {
        text-transform: uppercase;
        font-weight: 300;
        font-family: arial;
        font-size: 1.75rem;
        text-align: center
    }
    body {
        font-family: arial;
        font-weight: 400;
    }
    table tr td{
        border: 5px solid black;
background: blue;
color: white;
text-align: center;
padding: 10px;

    }
    table tr:first-child td {
background: red;
    }

    table  {
        border-spacing: 5px; 
    }
    </style>
</head>
<body>

<h1>tabela nr 1</h1>
    <table>
        <tr>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
        </tr>
        <tr>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
        </tr>
        <tr>
            <td>9</td>
            <td>10</td>
            <td>11</td>
            <td>12</td>
        </tr>
    </table>
    <h1>tabela nr 2</h1>
    <?php
    $sql = "SELECT * FROM pracownicy";
    $resultat = mysqli_query($polaczenie,$sql);
    echo "<table>";
    echo "<tr>";
    echo "<td>";
    echo "imie";
    echo "</td>";
    echo "<td>";
    echo "nazwisko";
    echo "</td>";
    echo "<td>";
    echo "wiek";
    echo "</td>";
    echo "<td>";
    echo "stanowisko";
    echo "</td>";
    echo "</tr>";
    while($row = mysqli_fetch_array($resultat)) {
        echo "<tr>";
        echo "<td>";
        echo $row["imie"];
        echo "</td>";
        echo "<td>";
        echo $row["nazwisko"];
        echo "</td>";
        echo "<td>";
        echo $row["wiek"];
        echo "</td>";
        echo "<td>";
        echo $row["stanowisko"];
        echo "</td>";
        echo "</tr>";
    }
    echo "</table>";
    
    ?>
</body>
</html>
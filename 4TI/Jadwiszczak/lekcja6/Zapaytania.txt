1.SELECT * FROM `pogoda` 
2.SELECT `nazwa` FROM `miasta` 
3.SELECT `opady`,`miasta_id` FROM `pogoda` WHERE `miasta_id`='2'
4.SELECT `temperatura_noc`,`data_prognozy` FROM `pogoda` WHERE `data_prognozy` > '2020-09-14' AND `data_prognozy` < '2020-09-30'
5.SELECT pogoda.`opady`, miasta.`id` FROM `pogoda` LEFT JOIN `miasta` ON pogoda.`opady`=miasta.`id` WHERE `miasta_id`='1'
6.SELECT `cisnienie` FROM `pogoda` WHERE `cisnienie` > '1000' AND `cisnienie` < '1018'
7.SELECT AVG(`temperatura_dzien`) FROM `pogoda`
8.SELECT MIN(`temperatura_noc`) FROM `pogoda`
9.SELECT MAX(`temperatura_dzien`) FROM `pogoda`
10.SELECT AVG(`opady`) FROM `pogoda`
11.SELECT COUNT(*) FROM `pogoda`
12.SELECT `temperatura_dzien` FROM `pogoda` ORDER BY `temperatura_dzien` ASC
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Klub wędkowania</title>
    <link rel="stylesheet" href="styl2.css">

    <?php
        $polaczenie = mysqli_connect("localhost", "root", "", "wedkowanie");
    ?>
    
</head>
<body>

    <section class="baner">
        <h2>Wędkuj z nami!</h2>
    </section>

    <section class="lewy">
        <img src="ryba2.jpg" alt="Szczupak">
    </section>

    <section class="prawy">
        <h3>Ryby spokojnego żeru (białe)</h3>

        <?php
            $sql = "SELECT `id`, `nazwa`, `wystepowanie` FROM `ryby` WHERE `styl_zycia` = '2'";
            $result = mysqli_query($polaczenie, $sql);

            while($row = mysqli_fetch_array($result)){
                echo $row['id'];
                echo ". ";
                echo $row['nazwa'];
                echo ", występuje w: ";
                echo $row['wystepowanie'];
                echo "<br>";
            }
        ?>

        <ol>
            <li><a href="https://wedkuje.pl/" target="_blank">Odwiedź także</a></li>
            <li><a href="http://www.pzw.org.pl/" target="_blank">Polski Związek Wędkarski</a></li>
        </ol>
    </section>

    <section class="stopka">
        <p>Stronę wykonał: 0000000000</p>
    </section>
    
</body>
</html>
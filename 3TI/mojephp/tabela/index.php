<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tabelka PHP</title>
    <?php
        $polaczenie = mysqli_connect("localhost", "root", "", "tabela_dane") or die("Error!");
        mysqli_query($polaczenie, "SET CHARSET utf8");
    ?>
    <style type="text/css">
    table {
        border-spacing: 5px;
        border-collapse: separate;
    }
    table tr td {
        border: 3px solid black;
        background: green;
        color: white;
        text-align: center;
        padding: 10px;
    }
    table tr:first-child td {
        background: lightgreen;
    }
    </style>
</head>
<body>
<h1>Tabela nr 1</h1>
    <table>
        <tr>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
        </tr>
        <tr>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
        </tr>
        <tr>
            <td>9</td>
            <td>10</td>
            <td>11</td>
            <td>12</td>
        </tr>
    </table>
    <h1>Tabela nr 2</h1>
    <?php
        $sql = "SELECT * FROM `pracownicy`";
        $resultat = mysqli_query($polaczenie, $sql);
        echo "<table>";
        echo "<tr>";
        echo "<td>";
            echo "Imie";
        echo "</td>";
        echo "<td>";
            echo "Nazwisko";
        echo "</td>";
        echo "<td>";
            echo "Wiek";
        echo "</td>";
        echo "<td>";
            echo "Stanowisko";
        echo "</td>";
    echo "</tr>";
        while ($row = mysqli_fetch_array($resultat)){
            echo "<tr>";
                echo "<td>";
                    echo $row["imie"];
                echo "</td>";
                echo "<td>";
                    echo $row["nazwisko"];
                echo "</td>";
                echo "<td>";
                    echo $row["wiek"];
                echo "</td>";
                echo "<td>";
                    echo $row["stanowisko"];
                echo "</td>";
            echo "</tr>";
        }
        echo "</table>";
    ?>
</body>
</html>